﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CellValue = Tic_Tac_Toe.TicTacToeBoard.CellValue;

namespace Tic_Tac_Toe
{
    public partial class Form2 : Form
    {
        Tic_Tac_Toe.TicTacToeBoard obj = new Tic_Tac_Toe.TicTacToeBoard();

        public Form2()
        {
            InitializeComponent();

        }
        private void Form2_Load(object sender, EventArgs e)
        {
            ResetGame();
        }
        private void ExitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            DisplayPlayerScore.Text = "";
            DisplayComputerScore.Text = "";
            DisplayTurn.Text = "";
            PlayerName.Text = "";
            ResetGame();
        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            Form1 F1 = new Form1();
            F1.Show();
            this.Hide();
        }

        private void DisplayPlayerScore_TextChanged(object sender, EventArgs e)
        {
            obj.get_PlayerScore();
        }

        private void DisplayComputerScore_TextChanged(object sender, EventArgs e)
        {
            obj.get_ComputerScore();
        }

        private void PlayerName_TextChanged(object sender, EventArgs e)
        {
            obj.set_PlayerName(PlayerName.Text);
            DisplayTurn.Text = obj.get_PlayerName();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (CheckName() == true)
            {
                Clicked(button1);
                DoMove(() => obj.MakeMove(1));
                DoMove(() => obj.MakeAIMove());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            if (CheckName() == true)
            {
                Clicked(button2);
                DoMove(() => obj.MakeMove(2));
                DoMove(() => obj.MakeAIMove());
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (CheckName() == true)
            {
                Clicked(button3);
                DoMove(() => obj.MakeMove(3));
                DoMove(() => obj.MakeAIMove());
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {

            if (CheckName() == true)
            {
                Clicked(button4);
                DoMove(() => obj.MakeMove(4));
                DoMove(() => obj.MakeAIMove());
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {

            if (CheckName() == true)
            {
                Clicked(button5);
                DoMove(() => obj.MakeMove(5));
                DoMove(() => obj.MakeAIMove());
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (CheckName() == true)
            {
                Clicked(button6);
                DoMove(() => obj.MakeMove(6));
                DoMove(() => obj.MakeAIMove());
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (CheckName() == true)
            {
                Clicked(button7);
                DoMove(() => obj.MakeMove(7));
                DoMove(() => obj.MakeAIMove());
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (CheckName() == true)
            {
                Clicked(button8);
                DoMove(() => obj.MakeMove(8));
                DoMove(() => obj.MakeAIMove());
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (CheckName() == true)
            {
                Clicked(button9);
                DoMove(() => obj.MakeMove(9));
                DoMove(() => obj.MakeAIMove());
            }
        }

        public bool CheckName()
        {
            if (PlayerName.Text == "")
            {
                MessageBox.Show("Please Enter Your Name !!");
                return false;
            }
            else
                return true;
        }

        public void ResetGame()
        {
            obj.set_ComputerScore(0);
            obj.set_PlayerScore(0);
            obj.set_PlayerName("");
            obj.set_Turn(1);

            StartNewGame();
        }

        private void DoMove(Func<bool> move)
        {
            if (move() == false)
                return;

            UpdateButtonLabels();

            CellValue winner = obj.CheckForWinner();

            if (winner != CellValue.BLANK)
                GameOver(winner);

            if (obj.GenerateAvailableMoves().Count == 0)
                GameOver(winner);
        }

        private void StartNewGame()
        {
            obj = new TicTacToeBoard();
            UpdateButtonLabels();
        }
        
        private void UpdateButtonLabels()
        {
            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    switch (obj.ValueAt(row, col))
                    {
                        case CellValue.BLANK:
                            if (row == 0 && col == 0)
                            {
                                button1.Text = "";
                                UnClick(button1);
                            }
                            else if (row == 0 && col == 1)
                            {
                                button2.Text = "";
                                UnClick(button2);
                            }
                            else if (row == 0 && col == 2)
                            {
                                button3.Text = "";
                                UnClick(button3);
                            }
                            else if (row == 1 && col == 0)
                            {
                                button4.Text = "";
                                UnClick(button4);
                            }
                            else if (row == 1 && col == 1)
                            {
                                button5.Text = "";
                                UnClick(button5);
                            }
                            else if (row == 1 && col == 2)
                            {
                                button6.Text = "";
                                UnClick(button6);
                            }
                            else if (row == 2 && col == 0)
                            {
                                button7.Text = "";
                                UnClick(button7);
                            }
                            else if (row == 2 && col == 1)
                            {
                                button8.Text = "";
                                UnClick(button8);
                            }
                            else if (row == 2 && col == 2)
                            {
                                button9.Text = "";
                                UnClick(button9);
                            }
                            break;
                        case CellValue.X:
                            if (row == 0 && col == 0)
                                button1.Text = "X";
                            else if (row == 0 && col == 1)
                                button2.Text = "X";
                            else if (row == 0 && col == 2)
                                button3.Text = "X";
                            else if (row == 1 && col == 0)
                                button4.Text = "X";
                            else if (row == 1 && col == 1)
                                button5.Text = "X";
                            else if (row == 1 && col == 2)
                                button6.Text = "X";
                            else if (row == 2 && col == 0)
                                button7.Text = "X";
                            else if (row == 2 && col == 1)
                                button8.Text = "X";
                            else if (row == 2 && col == 2)
                                button9.Text = "X";
                            break;
                        case CellValue.O:
                            if (row == 0 && col == 0)
                                button1.Text = "O";
                            else if (row == 0 && col == 1)
                                button2.Text = "O";
                            else if (row == 0 && col == 2)
                                button3.Text = "O";
                            else if (row == 1 && col == 0)
                                button4.Text = "O";
                            else if (row == 1 && col == 1)
                                button5.Text = "O";
                            else if (row == 1 && col == 2)
                                button6.Text = "O";
                            else if (row == 2 && col == 0)
                                button7.Text = "O";
                            else if (row == 2 && col == 1)
                                button8.Text = "O";
                            else if (row == 2 && col == 2)
                                button9.Text = "O";
                            break;
                    }
                }
            }
        }

        private void GameOver(CellValue winner)
        {
            string result;

            if (winner != CellValue.BLANK)
            {
                result = "Player " + winner.ToString() + " has won the game!";
                if (winner == CellValue.X)
                {
                    obj.set_PlayerScore(obj.get_PlayerScore() + 1);
                    DisplayPlayerScore.Text = obj.get_PlayerScore().ToString();
                }
                else if (winner == CellValue.O)
                {
                    obj.set_ComputerScore(obj.get_ComputerScore() + 1);
                    DisplayComputerScore.Text = obj.get_ComputerScore().ToString();
                }

            }
            else
                result = "The game was a tie!";

            MessageBox.Show(result);
            StartNewGame();
        }

        private void DisplayComputerScore_Click(object sender, EventArgs e)
        {

        }

        private void Clicked(Button b)
        {
            b.BackColor = Color.DarkGray;
            b.Enabled = false;
        }
        private void UnClick(Button c)
        {
            c.BackColor = Color.Gainsboro;
            c.Enabled = true;
        }


    }
}
