﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tic_Tac_Toe
{
    public partial class Form1 : Form
    {
        Form2 F = new Form2();
        TicTacToeBoard T = new TicTacToeBoard();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            T.set_level(1);
            F.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            T.set_level(2);
            F.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            T.set_level(3);
            F.Show();
            this.Hide();            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
